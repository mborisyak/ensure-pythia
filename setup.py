"""
  Utility package for Pythia Mill
"""

from setuptools import setup

from codecs import open
import os

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description = f.read()

setup(
  name='ensurepythia',

  version='8.3.03',

  description="""Locates or install Pythia 8 library.""",

  long_description = long_description,

  url='https://gitlab.com/mborisyak/ensure-pythia',

  author='Maxim Borisyak',
  author_email='mborisyak at hse dot ru',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'mborisyak at hse dot ru',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',

    'Intended Audience :: Science/Research',

    'Topic :: Scientific/Engineering :: Physics',

    'License :: OSI Approved :: MIT License',

    'Programming Language :: Python :: 3',
  ],

  keywords='Pythia',

  packages=['ensurepythia'],
)