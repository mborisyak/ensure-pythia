import os

__all__ = [
  'compilation_options',
  'ensure_pythia',
  'locate_pythia',
  'load_pythia_lib'
]

SHARED_ENV_VARIABLES = [
  'LD_LIBRARY_PATH',
  'LIBRARY_PATH'
]

INCLUDE_ENV_VARIABLES = [
  'INCLUDE_PATH',
  'C_INCLUDE_PATH',
  'C_PATH',
]

COMPILATION_OPTIONS = [
  '-Ofast', '-D_GLIBCXX_USE_CXX11_ABI=1', '-std=c++11', '-pedantic', '-W', '-Wall', '-Wshadow', '-fPIC'
]

SHARED_NAME = 'libpythia8.so'

INCLUDE_DIR = 'Pythia8'
INCLUDE_NAME = 'Pythia.h'

SHARE_DIR = 'Pythia8'
SHARE_NAME = 'xmldoc'

PYTHIA_URL = 'http://home.thep.lu.se/~torbjorn/pythia8/pythia8303.tgz'

here = os.path.dirname(__file__)

def ensure_pythia(n_jobs=None):
  lib_dir, include_dir, share_dir = locate_pythia()
  if lib_dir is None or include_dir is None or share_dir is None:
    return install_pythia(here, n_jobs=n_jobs)
  else:
    return lib_dir, include_dir, share_dir

def load_pythia_lib():
  lib_dir, _, _ = locate_pythia()

  from ctypes import cdll
  return cdll.LoadLibrary(os.path.join(lib_dir, 'libpythia8.so'))

def locate_pythia():
  def check_pythia_lib(path):
    if not os.path.isdir(path):
      return False

    return SHARED_NAME in os.listdir(path)

  def check_pythia_include(path):
    if not os.path.isdir(path):
      return False

    if INCLUDE_DIR in os.listdir(path):
      return INCLUDE_NAME in os.listdir(os.path.join(path, INCLUDE_DIR))
    else:
      return False

  def check_pythia_share(path):
    if not os.path.isdir(path):
      return False

    if SHARE_DIR in os.listdir(path):
      return SHARE_NAME in os.listdir(os.path.join(path, SHARE_DIR))
    else:
      return False

  env = os.environ

  library_directories = [
    directory

    for env_var in SHARED_ENV_VARIABLES
    if env_var in env

    for directory in env.get(env_var).split(':')
    if len(directory) > 0
  ] + [
    os.path.join(here, 'pythia/lib')
  ]

  include_directories = [
    directory

    for env_var in INCLUDE_ENV_VARIABLES
    if env_var in env

    for directory in env.get(env_var).split(':')
    if len(directory) > 0
  ] + [
    os.path.join(here, 'pythia/include')
  ]

  lib_locations = list(set([
    directory
    for directory in library_directories
    if check_pythia_lib(directory)
  ]))

  include_locations = list(set([
    directory
    for directory in include_directories
    if check_pythia_include(directory)
  ]))

  def get_share(include_dir):
    basename = os.path.dirname(include_dir)
    return os.path.join(basename, 'share')

  share_locations = list(set([
    get_share(include_dir)
    for include_dir in include_locations
    if check_pythia_share(get_share(include_dir))
  ]))

  if len(lib_locations) > 0:
    lib_location = lib_locations[-1]
  else:
    lib_location = None

  if len(include_locations) > 0:
    include_location = include_locations[-1]
  else:
    include_location = None

  if len(share_locations) > 0:
    share_location = share_locations[-1]
  else:
    share_location = None

  return lib_location, include_location, share_location

def compilation_options():
  return COMPILATION_OPTIONS

def install_pythia(target=None, n_jobs=None):
  if target is None:
    target = here

  if n_jobs is None:
    n_jobs = os.cpu_count() + 1

  def check_call(args):
    import subprocess as sp
    p = sp.run(args=args, stdout=sp.PIPE, stderr=sp.PIPE)

    if p.returncode != 0:
      print(p.stdout)
      print(p.stderr)
      raise Exception('%s step has failed!' % (' '.join(args)))

  from warnings import warn
  import urllib.request
  from urllib.parse import urlparse

  warn('Downloading Pythia from %s' % (PYTHIA_URL, ))
  response = urllib.request.urlopen(PYTHIA_URL)
  archive_name = os.path.basename(urlparse(PYTHIA_URL).path)
  archive_dir, _ = archive_name.split('.')
  data = response.read()

  import tempfile

  with tempfile.TemporaryDirectory() as tmp:
    archive_path = os.path.join(tmp, archive_name)
    with open(archive_path, 'wb') as f:
      f.write(data)

    import tarfile
    with tarfile.open(archive_path, mode='r|gz') as f:
      f.extractall(tmp)

    cwd = os.getcwd()
    source_path = os.path.join(tmp, archive_dir)
    os.chdir(source_path)

    pythia_install_path = os.path.join(target, 'pythia')

    options = ' '.join(COMPILATION_OPTIONS)
    common_options = "--cxx-common='%s'" % (options, )
    shared_options = "--cxx-shared='%s -shared'" % (options, )

    warn('Configuring Pythia:\n%s\n%s' % (common_options, shared_options))
    check_call([
      'bash',
      'configure',
      common_options,
      shared_options,
      "--enable-shared",
      "--prefix=%s" % (pythia_install_path, )
    ])

    warn('Configuring Pythia')

    warn('Compiling Pythia')
    check_call(['make', '-j%d' % (n_jobs, )])

    warn('Installing Pythia')
    check_call(['make', 'install'])

    os.chdir(cwd)

    return os.path.join(pythia_install_path, 'lib'), \
           os.path.join(pythia_install_path, 'include'), \
           os.path.join(pythia_install_path, 'share')