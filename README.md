# ensure-pythia

Locates or installs Pythia8 library.

## Usage

This package provides 4 functions:
- `compilation_options()`: retuns a list of `gcc` options `Pythia` is/will be compiled;
this does not take into account previous/manual `Pythia` installations.
- `ensure_pythia()`: returns tuple `(shared library location, include files location)`.
  If any of both is not found, downloads and compiles Pythia8, returns location of the installation.
- `locate_pythia()`: similar to `ensure_pythia` but does not install Pythia in case it is not found --- returns `None`s instead.
- `load_pythia_lib()`: tries to load `libpythia8.so` into memory. 

During installation of a package, one can use:
```python
lib_dir, include_dir = ensure_pythia()
```
To compile against Pythia (don't forget to add `ensurepythia` to `install_requires` option of your `setup.py`).

Inside code either use `load_pythia_lib()` or add library location to `LD_LIBRARY_PATH` variable.

This package installs Pythia inside itself.
